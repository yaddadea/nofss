# About
This is the code repository for the Neural Order-first Split-second method for solving the Capacitated vehicle routing problem (CVRP). Our method consists of an encoder-decoder architecture that implicitely solves CVRP instances by generating giant tours.
These giant tours are then evaluated using an exact oracle called split.
Contrary to other construction based approaches, our method builds a CVRP solution candidate in O(n) while relying on a dynamic programming based oracle in order to evaluate the quality of the solution.

# Model

Our model consists of a stack of GCN layer with relu activation in between for the encoder.
It outputs a representation of each of the clients and depot in a *d*-dimention space and a global representation of the instance computed using a pooling layer on the instance representation.
We then define a decoder network that will loop over until all clients have been routed in the giant tour.
For each step *t* we use a GRU recurrent cell where each of the previously selected clients is passed in.
This gives us, at each time step *t*, a  partial representation of the giant tour. At *t=0*, we use the depot as input for the GRU cell.
The global instance representation, the depot representation and the recurrent cell output are then passed concatenated and passed into a linear embedding layer to output a query vector.
The query vector is then used with the clients reprsentations in order to compute attention scores using a dot-product attention.
The scores are converted into probabilities using a softmax module.

# Training

We trained our model using REINFORCE with greedy rollout baseline on 100,000 CVRP instances on 3 instance sizes : 20, 50 and 100. This is the same training and evaluation protocol we can find in the litterature.

# Instance generation
We generated instances with repsectively 20, 50 and 100 clients. We have 100000 instances for training, 10000 for validation and 10000 for test.
Instance coordinates are sampled using a uniform distribution in $[0,1] \times [0,1]$. Demands are generated using a uniform distribution $\mathcal{U}(\{1,...,9\})$.
Vehicles capacity are 30, 40 and 50 respectively for each problem size.
In addition to this generation, we construct instance graphs the will be processed by the GCN encoder.
For this, we do not consider complete graphs by "sparcifying" them.
Our sparcification procedure consists of keeping for each client in the instance its $k$ nearest neighbors. We set $k$ to be the average number of clients in a route computed as the total number of clients divided by the lower bound of number of routes (i.e $k=\frac{n}{m}$ with $m=\lceil\frac{\sum_{i=1}^{n} dem_i}{C}\rceil$).

# How to use it
## Data generation
Data generation is handled by the `load_data.py` script. It will generate respectively 100000, 10000 and 10000 training, validation and test instances of size 20, 50 and 100.
The folder data has the following structure
`data/{train/valid/test}/{20/50/100}/{raw/processed}`

- The raw folder corresponds to the raw data generated before they are processed by torch_geometric.
- The processed folder corresponds to the processed data by torch_geometric. Please note that torch_geometric dataloader will automatically look into processed folder when performing training/validation/test. If it exists, it will not generate it again.

To generate the data, please run the following command:
```{bash}
python load_data.py
```

It will automatically create a folder named data that will store all the instances.

## Model training
Configuration files for training are specified in `configs` folder.
The folder has the following structure :
`configs/{encoder_type}/vrp{graph_size}.json`
where:
- `encoder_type` is either GAT, GCN or Transformer
- `graph_size` is either 20, 50 or 100

To train the model, please run the following command :

```{bash}
python train_seq.py -c configs/configs/{encoder_type}/vrp{graph_size}.json -d data
```

Example:
```{bash}
python train_seq.py -c configs/configs/GAT/vrp20.json -d data
```

When training, a log folder will be automatically generated with the following structure
`results/{graph_encoder}/{graph_size}`. It will contain a csv log file with the losses per epoch and the average tour
lengths in validation, model checkpoint `.pt` and `.png` files of different plots.


## Model evaluation

To evaluate a model, use either eval.py or evalSample.py script.

- eval.py serves to evaluate a model in a greedy decoding mode, i.e. the next client to add to the giant tour corresponds to the client with the highest probability. To use it, run in a terminal the following:

```{bash}
python eval.py -c results/{graph_encoder}/{graph_size}_{time}/configuration.json -d data -f results/{graph_encoder}/{graph_size}/{checkpoint}.pt
```

Example:

```{bash}
python eval.py -c results/gat/20_1674063341.9668684/configuration.json -d data -f results/gat/20_1674063341.9668684/RL_VRP_20_Epoch_100.pt
```

NB. To not override the different tests, the folder name is made by concatenating the instance size and the generation time.

- evalSample.py serves to evaluate a model in a sampling decoding mode, i.e. it will sample a liste of candidate solutions for each instance, and will choose the best among them. The execution from terminal is the same as in eval.py

```{bash}
python evalSample.py -c results/{graph_encoder}/{graph_size}_{time}/configuration.json -d data -f results/{graph_encoder}/{graph_size}/{checkpoint}.pt
```

NB. evalSample.py uses ASSplitDec.py which is a slight adaptation of SequentialDecoder.py in order to generate B candidate solution at each step. Next implementations should handle this in SequentialDecoder.py.

Both evalations result on a CSV file containing instance name, solution length, and time required to generate the solution.
