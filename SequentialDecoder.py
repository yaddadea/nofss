import torch
import torch.nn as nn
from torch.distributions import Categorical

# This class contains the main classes and methods to define the decoder
class SequentialDecoder(nn.Module):

    def __init__(self, embedding_dim, clipping, decode_mode='sample'):

        super(SequentialDecoder, self).__init__()
        self.clipping = clipping
        self.decode_mode = decode_mode
        self.embedding_dim = embedding_dim
        self.scale = embedding_dim ** 0.5


        self.gru = nn.GRU(embedding_dim, embedding_dim, batch_first=True)

        self.clt_emb = nn.Linear(embedding_dim, embedding_dim)
        self.query_emb = nn.Sequential(
            nn.Linear(3 * embedding_dim, embedding_dim),
            nn.ReLU(),
            nn.Linear(embedding_dim, embedding_dim))


        self.CrossEntropy = nn.CrossEntropyLoss(reduction='none')

    def greedy_decoding(self, softmaxed_logits):
        """
        :param softmaxed_logits: [batch_size, n]
        :return: probas [batch_size],  city_index: [batch_size, 1]
        """
        probas, city_index = torch.max(softmaxed_logits, dim=1)

        return probas, city_index.view(-1, 1)

    def sample_decoding(self, softmaxed_logits):
        """
        :param softmaxed_logits: [ batch_size, seq_len]
        :return: probas [batch_size],  city_index: [batch_size, 1]
        """
        batch_size = softmaxed_logits.size(0)
        m = Categorical(softmaxed_logits)  # distri probs
        city_index = m.sample()
        probas = softmaxed_logits[[i for i in range(batch_size)], city_index]

        return probas, city_index.view(-1, 1)

    def set_decode_mode(self, decode_mode):
        self.decode_mode = decode_mode

    def forward(self, encoder_output, demands=None, cap=None):
        """
        :param encoder_output: dim [batch_size, N_clients + 1, embedding_dim]
        :param device : either cuda or cpu
        :return: solution : the solutions
        :return: Future cities log probabilities
        """
        device = encoder_output.device
        batch_size, N_clients, _ = encoder_output.size()
        N_clients = N_clients - 1  # exclude the depot

        clients = self.clt_emb(encoder_output[:, 1:, :])  # [batch_size, N_clients, embedding_dim]
        depot = encoder_output[:, 0, :].unsqueeze(1)  # [batch_size, 1, embedding_dim]
        
        # [batch_size, 1, embedding_dim] mean according to the 1st dim
        graph_repr = encoder_output.mean(dim=-2).unsqueeze(1)

        mask = torch.zeros(size=[batch_size, N_clients], device=device).bool()  # initialised to False

        solutions = torch.tensor([], dtype=torch.long, device=device)

        log_proba = torch.zeros(batch_size, dtype=torch.float, device=device)

        last_visited_clt = depot

        for t in range(N_clients):
            _, last_visited_clt = self.gru(last_visited_clt)
            last_visited_clt = last_visited_clt.permute(1, 0, 2)  # (1, bs, embedding_dim) -> (bs, 1, embedding_dim)
            # [bs, 1, 3*embedding_dim]
            query = torch.cat((graph_repr, depot, last_visited_clt), dim=2)

            query = self.query_emb(query)  # (bs, 1, embedding_dim)

            # [bs, 1, embedding_dim] * [bs, N_clients, embedding_dim]
            # u_c dim : [batch_size, N_clients]
            u_c = torch.matmul(query, torch.transpose(clients, dim0=1, dim1=2)).squeeze(1)

            u_c = self.clipping * torch.tanh(u_c / self.scale) #+ torch.clip(torch.randn(N_clients), 0, 1)

            # u_c dim: [batch_size, N_Cities]
            # mask dim: [batch_size, N_Cities]

            #u_c = torch.log(u_c)
            u_c = u_c.masked_fill(mask, float('-inf'))
            proba =torch.softmax(u_c, dim=1)
            if self.decode_mode == 'sample':
                prob, clt_index = self.sample_decoding(proba)
            elif self.decode_mode == 'greedy':
                prob, clt_index = self.greedy_decoding(proba)

            mask = mask.scatter(1, clt_index, True)

            log_proba += torch.log(prob) 

            solutions = torch.cat((solutions, clt_index + 1), dim=1)

            last_visited_clt = clients[[i for i in range(batch_size)], clt_index.view(-1), :].unsqueeze(1)
            last_visited_clt = torch.cat((last_visited_clt, depot), dim=1)


        return log_proba, solutions
