import torch.nn as nn

class SeqModel(nn.Module):
    def __init__(self, encoder, decoder, nb_nodes, embedding_dim):
        super(SeqModel, self).__init__()
        self.nb_nodes = nb_nodes
        self.embedding_dim = embedding_dim
        self.encoder = encoder
        self.decoder = decoder

    def forward(self, data, demands=None, cap=None):
        """

        :param data: torch_geometric Data class
        :return: log_prob : [batch_size],
                 solutions : [batch_size, N_clients]
        """
        x = self.encoder(data)
        encoder_output = x.reshape(-1, self.nb_nodes, self.embedding_dim)

        log_prob, solution = self.decoder(encoder_output, demands, cap)
        return log_prob, solution

    def set_decode_mode(self, decode_mode):
        self.decoder.set_decode_mode(decode_mode)