from torch_geometric.data import Dataset
import torch
import libpysal
from torch_geometric.data import Data
from torch_geometric.loader import DataLoader
import os
from tqdm import tqdm

CAPACITIES = {
    20: 30,
    50: 40,
    100: 50,
}


class VRPDataset(Dataset):
    def __init__(self, root, transform=None, pre_transform=None, mode=None):
        self.processed = os.listdir(f'{root}/raw')
        #self.processed.remove("pre_transform.pt")
        #self.processed.remove("pre_filter.pt")

        super().__init__(root, transform, pre_transform)
        self.root = root
        self.transform = transform
        self.pre_transform = pre_transform
        self.mode = mode
    
    @property
    def raw_file_names(self):
        return self.processed

    @property
    def processed_file_names(self):
        return self.processed

    def len(self):
        return len(self.processed_file_names)

    def process(self):
        raw_files = self.raw_file_names
        processed_file_names = self.processed_file_names

        for raw_file in raw_files:
            idx = raw_file.split("_")[1].split('.')[0]
            raw_path = os.path.join(self.raw_dir, raw_file)
            raw_instance = torch.load(raw_path)
            demands = raw_instance["demands"]  # (num_nodes)
            angles = raw_instance["angles"]  # (num_nodes)
            coord = raw_instance["coord"]  # (num_nodes, 2)
            nb_clt = demands.size(0) - 1  # -1 to omit the depot
            # determine the number of nearest neighbors (k)
            k = nb_clt / torch.ceil(torch.div(demands.sum(-1), CAPACITIES[nb_clt])).item()
            # determine the k nearest neighbors for each client
            knn = libpysal.weights.KNN(coord.numpy(), k=k)
            knn_graph = knn.to_networkx()
            # add for each client a connexion to the depot
            #edges_to_add = [(0, i + 1) for i in range(nb_clt)]
            #knn_graph.add_edges_from(edges_to_add)
            #edges_to_add = [(i + 1, 0) for i in range(nb_clt)]
            #knn_graph.add_edges_from(edges_to_add)
            # build the torch geometric Data structure
            # x contains the node feature (x,y) coordinates, normalized demands, angle
            x = torch.cat((coord, torch.div(demands,CAPACITIES[nb_clt]).unsqueeze(-1)), dim=-1)
            x = torch.cat((x, angles.unsqueeze(-1)), dim=-1)  # (num_nodes, 4)
            # edge_index contains the list of all indexes
            edge_index = torch.tensor([], dtype=torch.long)
            for e in knn_graph.edges:
                edge_index = torch.cat((edge_index, torch.tensor(e)), dim=0)
            edge_index = edge_index.reshape(-1, 2).t().contiguous()  # (2, num_edges)

            # edge_attr contains the edge_attributes
            # (num_edges, num_edge_features)
            edge_attr = torch.tensor([])
            for ind, (u, v) in enumerate(knn_graph.edges()):
                dx = coord[u][0] - coord[v][0]
                dy = coord[u][1] - coord[v][1]
                dist_uv = (dx * dx + dy * dy) ** 0.5
                if ind == 0:
                    edge_attr = torch.tensor([dist_uv])
                else:
                    edge_attr = torch.cat((edge_attr, torch.tensor([dist_uv])), dim=0)

            edge_attr = edge_attr.reshape(-1, 1)  # # (num_edges, 1)

            data = Data(x=x, edge_index=edge_index, edge_attr=edge_attr)
            torch.save({
                "data": data,
                "demands": demands
            }, os.path.join(self.processed_dir, f"data_{idx}.pt"))



    def get(self, idx):
        checkpoint = torch.load(os.path.join(self.processed_dir, self.processed_file_names[idx]))
        data = checkpoint["data"]
        demands = checkpoint["demands"]
        if self.mode == "eval":
            return data, demands, self.processed_file_names[idx]
        return data, demands


def make_data(folder, size, nb_samples):
    for i in tqdm(range(nb_samples)):
        instance = torch.FloatTensor(size + 1, 2).uniform_(0, 1)
        demands = torch.randint(low=0, high=9, size=(size + 1,), dtype=torch.float32) + 1.0
        demands[0] = 0
        angles = torch.atan2(instance[:, 1] - instance[0, 1],
                             instance[:, 0] - instance[0, 0])

        torch.save({"coord": instance,
                    "demands": demands,
                    "angles": angles
                    }, os.path.join(os.path.join(folder, str(size), "raw"), f"data_{i}.pt"))


if __name__ == "__main__":

    torch.manual_seed(1234)

    data_folder = "data"
    train_folder = os.path.join(data_folder, "train")
    valid_folder = os.path.join(data_folder, "valid")
    test_folder = os.path.join(data_folder, "test")

    if not os.path.isdir(data_folder):
        os.makedirs(train_folder)
        os.makedirs(valid_folder)
        os.makedirs(test_folder)

    sizes = [20, 50, 100]
    folders = [train_folder, valid_folder, test_folder]
    num_samples = [100_000, 10_000, 10_000]
    #num_samples = [100, 100, 100]

    for size in sizes:
        for folder, nb_samples in zip(folders, num_samples):
            if not os.path.isdir(os.path.join(folder, str(size), "raw")):
                os.makedirs(os.path.join(folder, str(size), "raw"))

            make_data(folder, size, nb_samples)

    VRPDataset("data/train/20")
    VRPDataset("data/valid/20")
    VRPDataset("data/test/20")
