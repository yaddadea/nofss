import torch
from torch.optim import AdamW
from SeqModel import SeqModel
from load_data import VRPDataset
from torch_geometric.loader import DataLoader, DataListLoader
from tqdm import tqdm
from scipy.stats import ttest_rel
import time
import numpy as np
import matplotlib.pyplot as plt
import csv
import os
from SequentialDecoder import SequentialDecoder
from Encoders import GCNEncoder, GATEncoder, TransformerEncoder
from load_data import CAPACITIES
import json
import argparse
import gc
import ctypes
from functools import reduce
from operator import add
from torch.optim.lr_scheduler import StepLR

def split(capacity, demands, nbPoints, sequence, locations):
    nbp = nbPoints
    locations =  reduce(add, locations)
    cap = ctypes.c_int32(capacity)
    nbPoints = ctypes.c_int32(nbPoints)

    locs = tloc(*locations)

    tdem = arr_dem(*demands)
    permutation = arr_seq(*sequence)

    sol = splitter.algo(cap, tdem, nbPoints, permutation, locs)
    tour_length = sol

    del locs, tdem, permutation

    return tour_length


class Trainer:
    def __init__(self, batch_size, train_dataset, val_dataset, n_epochs, learning_rate, encoder,
                 graph_size, embedding_dim, edge_dim,
                 n_layers, n_heads, dropout, C, decode_mode,
                 freq_save, freq_log, device, config):

        self.config = config
        self.train_dataset = train_dataset
        self.val_dataset = val_dataset
        self.epoch_start = 0
        self.edge_dim = edge_dim
        self.n_epochs = n_epochs
        self.freq_save = freq_save
        self.freq_log = freq_log
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.n_layers = n_layers
        self.n_heads = n_heads
        self.dropout = dropout
        self.C = C
        self.decode_mode = decode_mode
        self.graph_size = graph_size
        self.alpha = config["alpha"]

        if self.config["use_angle"]:
            self.num_node_features = train_dataset.num_node_features
        else:
            self.num_node_features = train_dataset.num_node_features - 1  # exclude angles

        if encoder is GCNEncoder :
            self.encoder = encoder(self.num_node_features, embedding_dim, n_layers, dropout)
            self.t_encoder = encoder(self.num_node_features, embedding_dim, n_layers, dropout)
        else:
            self.encoder = encoder(self.num_node_features, embedding_dim, edge_dim, n_layers, n_heads, dropout)
            self.t_encoder = encoder(self.num_node_features, embedding_dim, edge_dim, n_layers, n_heads, dropout)

        self.decoder = SequentialDecoder(embedding_dim, C, decode_mode)
        self.model = SeqModel(self.encoder, self.decoder, graph_size+1, embedding_dim)

        self.t_decoder = SequentialDecoder(embedding_dim, C, decode_mode)
        self.target_net = SeqModel(self.t_encoder, self.t_decoder, graph_size+1, embedding_dim)

        self.device = device
        self.model.to(device)
        self.target_net.to(device)



        self.target_net.load_state_dict(self.model.state_dict())

        self.optimizer = AdamW(self.model.parameters(), lr=learning_rate)

        log_file_name = f"vrp_{graph_size}-logs_{time.time()}.csv"

        if config["xp_name"] is not None:
            self.folder = os.path.join("results", self.config['encoder'], f"{self.graph_size}_{config['xp_name']}_{time.time()}")
        else:
            self.folder = os.path.join("results", self.config['encoder'], f"{self.graph_size}_{time.time()}")

        if not os.path.isdir(self.folder):
            os.makedirs(self.folder)

        with open(os.path.join(self.folder, 'configuration.json'), 'w') as outfile:
            json.dump(self.config, outfile)

        self.f = open(os.path.join(self.folder, log_file_name), 'w', newline='')
        self.log_file = csv.writer(self.f, delimiter=",")

        header = ["epoch", "losses_per_batch", "avg_tl_batch_train", "avg_tl_epoch_train", "avg_tl_epoch_val"]
        self.log_file.writerow(header)

        self.best_avg_tl = float("+inf")

        self.scheduler = StepLR(self.optimizer, step_size=2, gamma=0.996)

    def rl_train(self):
        train_dataloader = DataLoader(self.train_dataset, batch_size=self.batch_size, shuffle=True, num_workers=3)

        validation_dataloader = DataLoader(self.val_dataset, batch_size=self.batch_size, shuffle=True, num_workers=2)
        losses = []
        avg_tour_length_batch = []
        avg_tour_length_epoch = []
        avg_tl_epoch_val = []

        nb_batches = len(train_dataloader)
        cap = CAPACITIES[self.graph_size]

        for epoch in range(self.epoch_start, self.n_epochs):
            self.scheduler.step()
            all_tour_lengths = torch.tensor([], dtype=torch.float32).to(self.device)

            self.model.set_decode_mode("sample")

            self.model.train()

            for batch_id, (data, demands) in enumerate(tqdm(train_dataloader)):
                if not self.config["use_angle"]:
                    data.x = data.x[:, 0:3]

                data = data.to(self.device)
                log_prob, solution = self.model(data)
                locations = data.x[:, :2].reshape(-1, self.graph_size+1, 2)

                with torch.no_grad():
                    _, rollout_sol = self.target_net(data)

                    tour_lengths = torch.tensor(list(
                    map(lambda dem, sol, location: split(cap, dem,
                                                         self.graph_size+1, sol.tolist(),
                                                         location.cpu().tolist()), demands.long().tolist(), solution, locations))
                    , device=self.device)

                    if epoch == 0:
                        if batch_id == 0:
                            baseline_tour_lengths = tour_lengths.detach().mean()
                        else:
                            baseline_tour_lengths = 0.8 * baseline_tour_lengths + (1-0.8) * tour_lengths.detach().mean()
                    else:
                        baseline_tour_lengths= torch.tensor(list(
                        map(lambda dem, sol, location: split(cap, dem,
                                                             self.graph_size+1, sol.tolist(),
                                                             location.cpu().tolist()), demands.long().tolist(), rollout_sol, locations))
                        , device=self.device)


                    advantage = tour_lengths - baseline_tour_lengths

                loss = advantage * log_prob + (-self.alpha * torch.exp(log_prob)*log_prob)
                loss = loss.mean()
                if batch_id % self.freq_log == 0:
                    print(
                        "\nEpoch: {}\tBatch: {}\nLoss: {}\nAverage tour length model : {}\nAverage tour length baseline : {}\n".format(
                            epoch, batch_id, loss.item(), tour_lengths.mean(), baseline_tour_lengths.mean()
                        ))

                self.optimizer.zero_grad()
                loss.backward()
                torch.nn.utils.clip_grad_norm_(self.model.parameters(), max_norm=1.)
                self.optimizer.step()

                # save data for plot
                losses.append(loss.item())
                avg_tour_length_batch.append(tour_lengths.mean().item())
                all_tour_lengths = torch.cat((all_tour_lengths, tour_lengths), dim=0)

            avg_tour_length_epoch.append(all_tour_lengths.mean().item())

            print("Validation and rollout update check\n")

            # t-test :

            self.model.set_decode_mode("greedy")
            self.target_net.set_decode_mode("greedy")
            self.model.eval()
            self.target_net.eval()
            with torch.no_grad():
                rollout_tl = torch.tensor([], dtype=torch.float32)
                policy_tl = torch.tensor([], dtype=torch.float32)

                for b_id, (data, demands) in enumerate(tqdm(validation_dataloader)):
                    if not self.config["use_angle"]:
                        data.x = data.x[:, 0:3]

                    data = data.to(self.device)
                    _, solution = self.model(data)
                    _, rollout_sol = self.target_net(data)
                    locations = data.x[:, :2].reshape(-1, self.graph_size + 1, 2)

                    tour_lengths = torch.tensor(list(
                    map(lambda dem, sol, location: split(cap, dem,
                                                         self.graph_size + 1, sol.tolist(),
                                                         location.cpu().tolist()), demands.long().tolist(),
                        solution, locations))
                    , device=self.device)

                    baseline_tour_lengths = torch.tensor(list(
                    map(lambda dem, sol, location: split(cap, dem,
                                                         self.graph_size + 1, sol.tolist(),
                                                         location.cpu().tolist()), demands.long().tolist(),
                        rollout_sol, locations))
                    , device=self.device)

                    rollout_tl = torch.cat((rollout_tl, baseline_tour_lengths.view(-1).cpu()), dim=0)
                    policy_tl = torch.cat((policy_tl, tour_lengths.view(-1).cpu()), dim=0)

                rollout_tl = rollout_tl.cpu().numpy()
                policy_tl = policy_tl.cpu().numpy()

                avg_ptl = np.mean(policy_tl)
                avg_rtl = np.mean(rollout_tl)

                avg_tl_epoch_val.append(avg_ptl.item())

                print(
                    "Average tour length by policy: {}\nAverage tour length by rollout: {}\n".format(avg_ptl, avg_rtl))

                self.log_file.writerow([epoch, losses[-nb_batches:],
                                        avg_tour_length_batch[-nb_batches:],
                                        avg_tour_length_epoch[-1],
                                        avg_ptl.item()
                                        ])
                self.f.flush()
                if avg_ptl < self.best_avg_tl:
                    self.best_avg_tl = avg_ptl
                    model_name = "RL_VRP_{}_best.pt".format(self.graph_size)
                    self.save_model(epoch, os.path.join(self.folder, model_name))

                if (avg_ptl - avg_rtl) < 0:
                    # t-test
                    _, pvalue = ttest_rel(policy_tl, rollout_tl)
                    pvalue = pvalue / 2  # one-sided ttest [refer to the original implementation]
                    if pvalue < 0.05:
                        print("Rollout network update...\n")
                        self.target_net.load_state_dict(self.model.state_dict())
            gc.collect()


            if epoch % self.freq_save == 0:
                model_name = "RL_VRP_{}_Epoch_{}.pt".format(self.graph_size, epoch)
                self.save_model(epoch, os.path.join(self.folder, model_name))

        model_name = "RL_VRP_{}_Epoch_{}.pt".format(self.graph_size, self.n_epochs)
        self.save_model(self.n_epochs, os.path.join(self.folder, model_name))


    def plot_stats(self, stats_array, plot_name, x_name, y_name):
        plt.plot(stats_array)
        plt.xlabel(x_name)
        plt.ylabel(y_name)
        plt.title(plot_name)
        plt.savefig(self.folder + plot_name + ".png")
        plt.cla()
        plt.clf()

    def save_model(self, epoch, model_name):
        torch.save({
            "epoch": epoch,
            "model": self.model.state_dict(),
            "baseline": self.target_net.state_dict(),
            "optimizer": self.optimizer.state_dict()
        }, model_name)


if __name__ == "__main__":

    def argparser():
        parser = argparse.ArgumentParser()
        parser.add_argument('-c', '--config', type=str, required=True,
                            help="Please specify a configuration file (json format)")
        parser.add_argument('-d', '--data', type=str, required=True,
                            help="Please specify the path to the data folder")
        parser.add_argument('-f', '--file', type=str, required=False,
                            help="Continue training using a .pt file")
        parser.add_argument('-n', '--name', type=str, required=False,
                            help="Experiment name")

        return parser


    args = argparser().parse_args()
    config_path = args.config
    data_path = args.data
    print("Loading configuration file\n")


    json_file = open(config_path, 'r')
    config = json.load(json_file)
    config["name"] = args.config
    config["prev"] = args.file
    config["xp_name"] = args.name
    print(config)
    graph_size = config["graph_size"]
    GRAPH_SIZE = graph_size
    n_epochs = config["n_epochs"]
    batch_size = config["batch_size"]
    n_layers = config["n_layers"]
    n_heads = config["n_heads"]
    embedding_dim = config["embedding_dim"]
    edge_dim = config["edge_dim"]
    decode_mode = config["decode_mode"]
    C = config["C"]
    dropout = config["dropout"]

    device = torch.device(config["device"])

    learning_rate = config["learning_rate"]
    seed = config["seed"]
    torch.manual_seed(0)
    freq_save = config["freq_save"]
    freq_log = config["freq_log"]

    encoders = {
        "gcn": GCNEncoder,
        "gat": GATEncoder,
        "transformer": TransformerEncoder
    }
    encoder = encoders[config["encoder"]]

    train_path = os.path.join(data_path, "train", str(graph_size))
    valid_path = os.path.join(data_path, "valid", str(graph_size))

    train_dataset = VRPDataset(train_path)
    val_dataset = VRPDataset(valid_path)

    trainer = Trainer(batch_size, train_dataset, val_dataset, n_epochs, learning_rate, encoder,
                      graph_size, embedding_dim, edge_dim,
                      n_layers, n_heads, dropout, C, decode_mode,
                      freq_save, freq_log, device, config
                      )
    k = (1./embedding_dim) ** 0.5
    # initialize all parameter values using a uniform distribution U(-k,k)
    for name, param in trainer.model.named_parameters():
        torch.nn.init.uniform_(param, -k, k)

    if args.file is not None:
        """
        checkpoint :
        {
            "epoch": epoch,
            "model": self.model.state_dict(),
            "baseline": self.target_net.state_dict(),
            "optimizer": self.optimizer.state_dict()
        }
        """
        checkpoint = torch.load(args.file, map_location=torch.device(config["device"]))
        print(checkpoint)
        trainer.epoch_start = checkpoint["epoch"]
        trainer.model.load_state_dict(checkpoint["model"])
        trainer.target_net.load_state_dict(checkpoint["baseline"])
        trainer.optimizer.load_state_dict(checkpoint["optimizer"])


    arr_dem = (ctypes.c_int32 * (GRAPH_SIZE + 1))
    arr_seq = (ctypes.c_int32 * GRAPH_SIZE)
    tloc = (ctypes.c_double * (2 * (GRAPH_SIZE + 1)))

    splitter = ctypes.cdll.LoadLibrary("./splitter/split_oracle.so")
    splitter.algo.argtypes = [ctypes.c_int32, arr_dem, ctypes.c_int32, arr_seq, tloc]
    splitter.algo.restype = ctypes.c_double   # ctypes.c_void_p

    trainer.rl_train()
