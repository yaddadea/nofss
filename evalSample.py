import torch
from torch.optim import AdamW
from SeqModel import SeqModel
from torch_geometric.loader import DataLoader
from tqdm import tqdm
import time
import csv
import os
from ASSplitDec import SequentialDecoder
from Encoders import GCNEncoder, GATEncoder, TransformerEncoder
import json
import argparse
import ctypes
from functools import reduce
from operator import add
from torch.optim.lr_scheduler import StepLR
from load_data import VRPDataset
from load_data import CAPACITIES

def split(capacity, demands, nbPoints, sequence, locations):
    nbp = nbPoints
    locations =  reduce(add, locations)
    cap = ctypes.c_int32(capacity)
    nbPoints = ctypes.c_int32(nbPoints)

    locs = tloc(*locations)

    tdem = arr_dem(*demands)
    permutation = arr_seq(*sequence)

    sol = splitter.algo(cap, tdem, nbPoints, permutation, locs)
    tour_length = sol

    del locs, tdem, permutation

    return tour_length


class Evaluator:
    def __init__(self,batch_size, graph_size, eval_dataset, steps, learning_rate, encoder,
                 embedding_dim, edge_dim,
                 n_layers, n_heads, dropout, C, decode_mode, device, file_name, config):

        self.config = config
        self.edge_dim = edge_dim
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.n_layers = n_layers
        self.n_heads = n_heads
        self.dropout = dropout
        self.C = C
        self.decode_mode = "sample"

        self.alpha = config["alpha"]
        self.graph_size = graph_size
        self.B = self.graph_size - 1

        self.steps = steps // self.B + 1
        self.eval_dataset = eval_dataset

        self.num_node_features = 4

        if encoder is GCNEncoder:
            self.encoder = encoder(self.num_node_features, embedding_dim, n_layers, dropout)
            self.t_encoder = encoder(self.num_node_features, embedding_dim, n_layers, dropout)
        else:
            self.encoder = encoder(self.num_node_features, embedding_dim, edge_dim, n_layers, n_heads, dropout)
            self.t_encoder = encoder(self.num_node_features, embedding_dim, edge_dim, n_layers, n_heads, dropout)

        self.decoder = SequentialDecoder(embedding_dim, C, self.B, decode_mode)
        self.model = SeqModel(self.encoder, self.decoder, graph_size+1, embedding_dim)

        self.device = device
        self.model.to(device)



        self.folder = os.path.join("results", "csv")

        if not os.path.isdir(self.folder):
            os.makedirs(self.folder)

        with open(os.path.join(self.folder, 'configuration.json'), 'w') as outfile:
            json.dump(self.config, outfile)


        log_file_name = file_name

        self.f = open(os.path.join(self.folder, log_file_name), 'w', newline='')
        self.log_file = csv.writer(self.f, delimiter=",")

        header = ["instance", "sol_length", "time"]
        self.log_file.writerow(header)

        self.f.flush()
        self.arr_steps = [i for i in range(self.steps)]
    def eval(self):

        eval_dataloader = DataLoader(self.eval_dataset, batch_size=self.batch_size, shuffle=True, num_workers=3)

        cap = CAPACITIES[self.graph_size]
        KAPA = torch.tensor([cap], dtype=torch.float, device=self.device)

        for b_id, (data, demands, fname) in enumerate(tqdm(eval_dataloader)):

            if not self.config["use_angle"]:
                data.x = data.x[:, 0:3]

            data = data.to(self.device)
            locations = data.x[:, :2].reshape(-1, self.graph_size + 1, 2)
            start = time.time()
            min_tls = list(map(lambda i: self.inner_eval(data, demands, KAPA, cap, locations), self.arr_steps))
            end = time.time()
            temps = (end - start)
            mmin = min(min_tls)
            #lignes = map(lambda fn, val: [fn, val.item(), temps], fname, tl)
            self.log_file.writerow([fname[0], mmin, temps])
            self.f.flush()

        self.f.close()

    def inner_eval(self, data, demands, KAPA, cap, locations):
        _, solution = self.model(data, demands.float().to(self.device), KAPA)

        tour_lengths = torch.tensor(list(
            map(lambda sol: split(cap, demands[0].long().tolist(),
                                                 self.graph_size + 1, sol.tolist(),
                                                 locations[0].cpu().tolist()), solution))
            , device=self.device)
        min_tl = min(tour_lengths).item()
        return min_tl


if __name__ == "__main__":

    def argparser():
        parser = argparse.ArgumentParser()
        parser.add_argument('-c', '--config', type=str, required=True,
                            help="Please specify a configuration file (json format)")
        parser.add_argument('-d', '--data', type=str, required=True,
                            help="Please specify the path to the data folder")
        parser.add_argument('-f', '--file', type=str, required=False,
                            help="Continue training using a .pt file")

        return parser


    args = argparser().parse_args()
    config_path = args.config
    data_path = args.data
    print("Loading configuration file\n")


    json_file = open(config_path, 'r')
    config = json.load(json_file)
    config["name"] = args.config
    config["prev"] = args.file
    print(config)
    graph_size = config["graph_size"]
    GRAPH_SIZE = graph_size
    n_epochs = config["n_epochs"]
    batch_size = 1
    n_layers = config["n_layers"]
    n_heads = config["n_heads"]
    embedding_dim = config["embedding_dim"]
    edge_dim = config["edge_dim"]
    decode_mode = config["decode_mode"]
    C = config["C"]
    dropout = config["dropout"]
    if config["device"] != "multi":
        device = torch.device(config["device"])
    else:
        device = config["device"]
    learning_rate = config["learning_rate"]
    seed = config["seed"]
    torch.manual_seed(0)
    freq_save = config["freq_save"]
    freq_log = config["freq_log"]
    steps = 1280
    encoders = {
        "gcn": GCNEncoder,
        "gat": GATEncoder,
        "transformer": TransformerEncoder
    }
    encoder = encoders[config["encoder"]]

    eval_path = os.path.join(data_path, "test", str(graph_size))


    eval_dataset = VRPDataset(eval_path, mode="eval")

    file_name = f"{graph_size}_{config['encoder']}.csv"
    evaluator = Evaluator(batch_size, graph_size, eval_dataset, steps, learning_rate, encoder, embedding_dim,
                          edge_dim, n_layers, n_heads, dropout, C, decode_mode, device, file_name, config)

    if args.file is not None:
        """
        checkpoint :
        {
            "epoch": epoch,
            "model": self.model.state_dict(),
            "baseline": self.target_net.state_dict(),
            "optimizer": self.optimizer.state_dict()
        }
        """
        checkpoint = torch.load(args.file, map_location=torch.device(config["device"]))
        #print(checkpoint)
        evaluator.model.load_state_dict(checkpoint["model"])
        #evaluator.model.load_state_dict(checkpoint["baseline"])




    arr_dem = (ctypes.c_int32 * (GRAPH_SIZE + 1))
    arr_seq = (ctypes.c_int32 * GRAPH_SIZE)
    tloc = (ctypes.c_double * (2 * (GRAPH_SIZE + 1)))

    splitter = ctypes.cdll.LoadLibrary("./splitter/split_oracle.so")
    splitter.algo.argtypes = [ctypes.c_int32, arr_dem, ctypes.c_int32, arr_seq, tloc]
    splitter.algo.restype = ctypes.c_double   # ctypes.c_void_p

    
    evaluator.eval()
