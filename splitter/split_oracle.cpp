#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#define MAXDISTVAL 999999999
double* minlong;
int* endprev ;

struct solution {
        double tourLength ;
        int* pred;
};

double split(int* permutation, int nbPoints, int* tdem, int KAPA, double* tloc);
//extern "C" struct solution* algo(int KAPA, int* tdem, int nbPoints, int* permutation, double *tloc);
extern "C" double algo(int KAPA, int* tdem, int nbPoints, int* permutation, double *tloc);

double split(int* permutation, int nbPoints, int* tdem, int KAPA, double* tloc)
 {
     int firstpos, pos, client, client_, demande;
     double dist, longtour;

     client_ = 0;    // depot
     demande = 0;
     dist = 0;
     for (pos = 0; pos < nbPoints-1; pos++)
     {
         client = permutation[pos];
         demande += tdem[client];
         if (demande <= KAPA)
         {
             //tdist[client_][client];
             dist += sqrt( pow(tloc[2*client_]-tloc[2*client],2) + pow(tloc[2*client_+1]-tloc[2*client+1],2) ) ;
             //minlong[pos] = dist + tdist[client][0];
             minlong[pos] = dist + sqrt(pow(tloc[0]-tloc[2*client],2) + pow(tloc[0+1]-tloc[2*client+1],2)) ;
             endprev[pos] = -1;
         }
         else
             goto suite;
         client_ = client;
     }
     return 1;
suite:
     for (; pos < nbPoints-1; pos++)
         minlong[pos] = MAXDISTVAL;

     for (firstpos=1; firstpos < nbPoints - 1; firstpos++)
     {
         client = permutation[firstpos];
         //dist    = tdist[0][client];
         dist      = sqrt(pow(tloc[0]-tloc[2*client],2) + pow(tloc[0+1]-tloc[2*client+1],2)) ;
         //longtour= dist+tdist[client][0];
         longtour = dist+ sqrt(pow(tloc[2*client]-tloc[0],2) + pow(tloc[2*client+1]-tloc[0+1],2)) ;
         if (minlong[firstpos-1]+longtour < minlong[firstpos])
         {
             minlong[firstpos] = minlong[firstpos-1]+longtour;
             endprev[firstpos] = firstpos-1;
         }
         client_    = client;
         demande = tdem[client];
         pos        = firstpos+1;
         while (pos < nbPoints - 1)
         {
             client = permutation[pos];
             demande += tdem[client];
             if (demande <= KAPA)
             {
                 //dist += tdist[client_][client];
                 dist   += sqrt( pow(tloc[2*client_]-tloc[2*client],2) + pow(tloc[2*client_+1]-tloc[2*client+1],2) ) ;
                 //longtour = dist + tdist[client][0];
                 longtour = dist + sqrt(pow(tloc[2*client]-tloc[0],2) + pow(tloc[2*client+1]-tloc[0+1],2)) ;
                 if (minlong[firstpos-1]+longtour < minlong[pos])
                 {
                     minlong[pos] = minlong[firstpos-1]+longtour;
                     endprev[pos] = firstpos-1;
                 }
             }
             else
                 goto nextpos;
             pos++;
             client_ = client;
         }
     nextpos:;
     }
     return minlong[nbPoints-2];
 }


extern "C"{
//struct solution*
double algo(int KAPA,
          int* tdem,
          int nbPoints,
          int* permutation,
		  double *tloc)
{
	int maxTours;
	int demandeMin;
	int demandeMax;
	int nbTours;

	int somdemande;
	int i, j, minTours;
	// NBR TOURNEES	OPT
	demandeMin = demandeMax = somdemande = tdem[1];
	for (i = 2; i < nbPoints; i++)
	{
		j = tdem[i];
		somdemande += j;
		if (j > demandeMax) demandeMax = j;
		else
			if (j < demandeMin) demandeMin = j;
	}
    minTours = somdemande / KAPA;
	if (minTours * KAPA < somdemande) minTours++;
	j = KAPA % demandeMax;
	j = KAPA - j;
	maxTours = somdemande / j;
	if (maxTours * j < somdemande) maxTours++;

	//minlong = new double* [maxTours];
    minlong = (double*)malloc(nbPoints * sizeof(double));
    endprev = (int*)malloc(nbPoints * sizeof(int));
    for (i = 0; i < nbPoints; i++)
	{
		    endprev[i] = -1 ;
	}

    //struct solution *  sol = (struct solution*)malloc(sizeof(struct solution)) ;

    //sol->tourLength  = split(permutation, nbPoints, tdem, KAPA, tloc);
    double tourLength  = split(permutation, nbPoints, tdem, KAPA, tloc);
    //sol->pred = (int*)malloc(nbPoints * sizeof(int));

    //for(int t = 0 ; t < nbPoints ; t++) sol->pred[t] = endprev[t];

    free(minlong);
    free(endprev);

    return tourLength;
}

}