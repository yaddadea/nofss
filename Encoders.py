from torch_geometric.nn import GATConv, GCNConv, TransformerConv
import torch
from load_data import VRPDataset


class GATEncoder(torch.nn.Module):
    def __init__(self, num_node_features, embedding_dim, edge_dim, nb_layers, n_heads, dropout):
        super().__init__()
        self.embedding_dim = embedding_dim
        self.nb_layers = nb_layers
        self.num_node_features = num_node_features
        self.n_heads = n_heads
        self.edge_dim = edge_dim
        self.dropout = torch.nn.Dropout(dropout)
        self.convs = torch.nn.ModuleList([GATConv(num_node_features, embedding_dim, heads=n_heads, concat=False,
                                                  edge_dim=edge_dim)])
        self.convs.extend(
            [
                GATConv(embedding_dim, embedding_dim, heads=n_heads, concat=False, edge_dim=edge_dim)
                for _ in range(nb_layers - 2)  # -2 to omit the first and the last layer
            ]
        )
        self.last_conv = GATConv(embedding_dim, embedding_dim, heads=n_heads, concat=False, edge_dim=edge_dim)

    def forward(self, data):
        # edge_attr shape : (num_edges, 1)
        x, edge_index, edge_attr = data.x, data.edge_index, data.edge_attr
        for conv in self.convs:
            x = conv(x, edge_index, edge_attr)
            x = torch.relu(x)
            x = self.dropout(x)
        # (bs*num_nodes, embedding_dim)
        x = self.last_conv(x, edge_index, edge_attr)
        return x


class TransformerEncoder(torch.nn.Module):
    def __init__(self, num_node_features, embedding_dim, edge_dim, nb_layers, n_heads, dropout):
        super().__init__()
        self.embedding_dim = embedding_dim
        self.nb_layers = nb_layers
        self.num_node_features = num_node_features
        self.n_heads = n_heads
        self.edge_dim = edge_dim
        self.dropout = torch.nn.Dropout(dropout)
        self.convs = torch.nn.ModuleList([TransformerConv(num_node_features, embedding_dim, heads=n_heads, concat=False,
                                                          edge_dim=edge_dim)])
        self.convs.extend(
            [
                TransformerConv(embedding_dim, embedding_dim, heads=n_heads, concat=False, edge_dim=edge_dim)
                for _ in range(nb_layers - 2)  # -2 to omit the first and the last layer
            ]
        )
        self.last_conv = TransformerConv(embedding_dim, embedding_dim, heads=n_heads, concat=False, edge_dim=edge_dim)

    def forward(self, data):
        # edge_attr shape : (num_edges, 1)
        x, edge_index, edge_attr = data.x, data.edge_index, data.edge_attr

        for conv in self.convs:
            x = conv(x, edge_index, edge_attr)
            x = torch.relu(x)
            x = self.dropout(x)
        # (bs*num_nodes, embedding_dim)
        x = self.last_conv(x, edge_index, edge_attr)
        return x


class GCNEncoder(torch.nn.Module):
    def __init__(self, num_node_features, embedding_dim, nb_layers, dropout):
        super().__init__()
        self.embedding_dim = embedding_dim
        self.nb_layers = nb_layers
        self.num_node_features = num_node_features
        self.dropout = torch.nn.Dropout(dropout)
        self.convs = torch.nn.ModuleList([GCNConv(num_node_features, embedding_dim)])
        self.convs.extend(
            [
                GCNConv(embedding_dim, embedding_dim)
                for _ in range(nb_layers - 2)  # -2 to omit the first and the last layer
            ]
        )
        self.last_conv = GCNConv(embedding_dim, embedding_dim)
    def forward(self, data):
        x, edge_index, edge_weight = data.x, data.edge_index, data.edge_attr
        edge_weight = edge_weight.reshape(-1)  #(num_edges,)
        for conv in self.convs:
            x = conv(x, edge_index, edge_weight)
            x = torch.relu(x)
            x = self.dropout(x)
        # (bs*num_nodes, embedding_dim)
        x = self.last_conv(x, edge_index, edge_weight)
        return x



if __name__ == "__main__":
    from torch_geometric.loader import DataLoader

    dataset = VRPDataset("data/train/20")


    gcn = GCNEncoder(dataset.num_node_features,64,3,0.1)
    gat = GATEncoder(dataset.num_node_features, 64, 1, 3, 2, 0.1)
    transformer = TransformerEncoder(dataset.num_node_features, 64, 1, 3, 2, 0.1)
    dataloader = DataLoader(dataset, batch_size=32)
    print(len(dataloader))
    for data, demands in dataloader:
        out2 = transformer(data)
        print(data.x.shape)
        exit()
